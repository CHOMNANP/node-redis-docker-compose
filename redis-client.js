const redis = require('redis');
const {
  promisify
} = require('util');
const _ = require('lodash');

console.log("process.env.REDIS_URL", process.env.REDIS_URL);
const client = redis.createClient(process.env.REDIS_URL, { password: _.get(process.env, 'REDIS_PASSWORD', '') });
// const client = redis.createClient('http://localhost:6379');

module.exports = {
  ...client,
  getAsync: promisify(client.get).bind(client),
  setAsync: promisify(client.set).bind(client),
  keysAsync: promisify(client.keys).bind(client)
};