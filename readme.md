

# Objective

Running a nodejs http sever in a container with docker-compose.
The server requires to connect to a database outside the network.


### install docker and docker compose
https://docs.docker.com/compose/install/

### i. install and run project
1. git clone https://gitlab.com/CHOMNANP/node-redis-docker-compose.git && cd node-redis-docker-compose
2. chmod +x install.sh && ./install.sh 
3. cp .env.example .env
4. docker-compose up --build 


### ii. manual test
1. check the console from step i.4 above and see if it print the store the value "Successfully put and pull data with Redis"
2. run http://localhost:3000/ to see of the url is working



## What does it mean after following the step above?
It mean that you have successfully run a nodejs http server that run inside a container. The server is accessing to and external memory database outside the container which possible accessing to outside network as well.