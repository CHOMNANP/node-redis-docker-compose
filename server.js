require('dotenv').config();
const express = require('express');
const app = express();
const redisClient = require('./redis-client');

app.get('/', (req, res) => {
    return res.send('Hello Khmer Coder');
});

const APP_PORT = process.env.APP_PORT || 3000;
const tetingReisKey = 'testKey';
const testingValue = 'Successfully put and pull data with Redis'; //You can changethis value to test out

app.listen(APP_PORT, async () => {
    console.log(`Server listening on port ${APP_PORT}`);

    await redisClient.setAsync(tetingReisKey, testingValue);
    console.log("successfully put data into redis");
    const rawData = await redisClient.getAsync(tetingReisKey);
    console.log(`data reading from redis key '${rawData}'`);

});

// app.get('/store/:key', async (req, res) => {
//     const {
//         key
//     } = req.params;
//     const value = req.query;
//     await redisClient.setAsync(key, JSON.stringify(value));
//     return res.send('Success');
// });
// app.get('/:key', async (req, res) => {
//     const {
//         key
//     } = req.params;
//     const rawData = await redisClient.getAsync(key);
//     return res.json(JSON.parse(rawData));
// });